<?php
    require_once("db.php");

    //initialize variables
    $accountid = 0;
    $studentid = 0;
    $pwd = "";
    $uname = "";
    $accountbalance = 0;
    $deleteaccount = "";
    $makeadmin = "";
    $fname = "";
    $lname ="";
    $locationid = 0;
    $locname = "";
    $address = "";
    $floor = 0;
    $deletelocation = "";
    $addlocation = "";



    
    //set variables from form
    if(isset($_GET["accountid"])) $accountid=$_GET["accountid"];
    if (isset($_GET["studentid"])) $studentid = $_GET["studentid"];
    if (isset($_GET["fname"])) $fname = $_GET["fname"];
    if (isset($_GET["lname"])) $lname = $_GET["lname"];
    if (isset($_GET["password"])) $pwd = $_GET["password"];
    if (isset($_GET["username"])) $uname = $_GET["username"];
    if (isset($_GET["accountbalance"])) $accountbalance = $_GET["accountbalance"];
    if (isset($_GET["deleteaccount"])) $deleteaccount = $_GET["deleteaccount"];
    if (isset($_GET["makeadmin"])) $makeadmin = $_GET["makeadmin"];
    if (isset($_GET["locationid"])) $locationid = $_GET["locationid"];
    if (isset($_GET["locname"])) $locname = $_GET["locname"];
    if (isset($_GET["address"])) $address = $_GET["address"];
    if (isset($_GET["floor"])) $floor = $_GET["floor"];
    if (isset($_GET["deletelocation"])) $deletelocation = $_GET["deletelocation"];
    if (isset($_GET["addlocation"])) $addlocation = $_GET["addlocation"];


    //student - if not set, use previously stored variables
    $sql = "SELECT * FROM spacedout.student WHERE account_id='$accountid'";
    $result = $mydb->query($sql);
    $row = mysqli_fetch_array($result);
    if (empty($studentid)) $studentid = $row['student_id'];
    if(empty($accountbalance)) $accountbalance = $row['point_balance'];

    //admin - if not set, use previously stored variables
    $sql = "SELECT * FROM spacedout.login WHERE account_id='$accountid'";
    $result = $mydb->query($sql);
    $row = mysqli_fetch_array($result);
    if (empty($pwd)) $pwd = $row['pwd']; 
    if(empty($uname)) $uname = $row['uname'];
    if (empty($fname)) $fname = $row['fname']; 
    if (empty($lname)) $lname = $row['lname']; 

    //location - if not set, use previously stored variables
    $sql = "SELECT * FROM spacedout.location WHERE location_id='$locationid'";
    $result = $mydb->query($sql);
    $row = mysqli_fetch_array($result);
    if (empty($locname)) $locname = $row['location_name']; 
    if (empty($address)) $address = $row['address']; 
    if (empty($floor)) $floor = $row['floor']; 


    
    //update or delete account
    if  (!empty($makeadmin)){
        $sql = "UPDATE spacedout.login set isAdmin='1' WHERE account_id='$accountid'";
        $result = $mydb->query($sql);
        $sql = "DELETE FROM student WHERE account_id = $accountid";
        $result = $mydb->query($sql);

    }elseif (empty($deleteaccount)){
        $sql = "UPDATE spacedout.student set student_id='$studentid', point_balance='$accountbalance' WHERE account_id='$accountid' ";
        $result = $mydb->query($sql);
        $sql = "UPDATE spacedout.login set pwd='$pwd', uname='$uname', fname='$fname', lname='$lname' WHERE account_id='$accountid' ";
        $result = $mydb->query($sql);
    }else {
        $sql = "DELETE FROM student WHERE account_id = $accountid";
        $result = $mydb->query($sql);
        $sql = "DELETE FROM login WHERE account_id = $accountid";
        $result = $mydb->query($sql);
    }
     
    //update or delete location
    if  (empty($deletelocation) && empty($addlocation)){
        $sql = "UPDATE spacedout.location set location_name='$locname', address='$address', floor='$floor' WHERE location_id='$locationid' ";
        $result = $mydb->query($sql);
    }elseif(empty($addlocation)){
        $sql = "DELETE FROM location WHERE location_id = $locationid";
        $result = $mydb->query($sql);
    }else{

        $sql = "INSERT INTO spacedout.location (location_name, floor, address)
        VALUES ('$locname','$floor','$address')";
        $result = $mydb->query($sql);
    }
    header('Location: ManageApp.php');

     
?>
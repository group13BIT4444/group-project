<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src='https://d3js.org/d3.v4.min.js'></script>
    <meta charset="utf-8">
    <title>App Usage Analysis</title>
    <!-- Styling for Bar chart -->
    <style>

        .bar {
        fill: steelblue;
        }

        .bar:hover {
        fill: brown;
        }

        .axis--x path {
        display: none;
        }

    </style>
    <!-- Error Label styling -->
    <style>
        .errlabel {color:red};
    </style>

</head>

<!-- Code for Use Current Date button -->
<script> 
   function useCurrentDate(){ 
       var today = new Date();
       var month = today.getMonth()+1;
       var day = today.getDay()+1;
       if(month.toString().length<2){
           month = "0" + month;
           Number(month);
       }
       if(day.toString().length<2){
           day = "0" + day;
           Number(day);
       }
       var year = today.getFullYear();
        document.getElementById("endDate").value = year+"-"+month+"-"+day;
    }
</script>

<!-- Code for submit button ajax (incomplete - reroutes to a different page) -->
<script>
    function getContentArea(){
        var asyncRequest;
        var z = document.getElementById("contentArea");
        var startdate = document.forms[0].startdate.value;
        var enddate = document.forms[0].enddate.value;

        try {
          asyncRequest = new XMLHttpRequest();  //create request object
          asyncRequest.onreadystatechange=stateChange;
          var url="AppUsageAnalysisSubmit.php?startDate="+startdate&"endDate="+enddate;
          asyncRequest.open('GET',url,true);  // prepare the request
          asyncRequest.send(null);  // send the request
        }
        catch(exception) {alert("Request failed");
        }
 
        function stateChange(){
            if(asyncRequest.readyState==4 && asyncRequest.status==200) {
            document.getElementById("contentArea").innerHTML=
            asyncRequest.responseText;  // places text in contentArea
        }
      }
    }
</script>

<body>
    <div class="container-fluid">
        <!-- Nav Bar Code -->
        <div id="nav-placeholder">
            </div>
            <script>
            $(function() {
            $("#nav-placeholder").load("nav.php");
                });
        </script>
        <!-- code for page content -->
        <div class="container-fluid text-center">
            <div class="row content">
                <!-- code for side bar page links -->
                <div class="col-sm-2 sidenav">
                    <p><a href="AdminHome.php">Admin Home Page</a></p>
                    <p><a href="LocationAnalysis.php">Location Analysis</a></p>
                    <p><a href="ManageApp.php">Manage Application</a></p>
                </div>
                <!-- code for Welcome paragraph -->
                <div class="col-sm-8 text-left">
                    <h2>Welcome to the Usage Analysis Page!</h2>
                    <p>Please select a time frame you would like to get information on.</p>
                    <hr>
                </div>
            </div>
        </div>
            <!-- Code for form inputs -->
            <form class="form-horizontal" method="get" action="AppUsageAnalysisSubmit.php">
                <!--Code for start date -->
                <div class="form-group">
                    <label class="control-label col-sm-2" for="startDate">Start Date:</label>
                    <div class="col-sm-10">
                        <input type="date" name="startdate" class="form-control" id="startDate">
                    </div>
                </div>
                <!-- Code for end date -->
                <div class="form-group">
                    <label class="control-label col-sm-2" for="endDate">End Date:</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="enddate" id="endDate">
                        <?php // if($er) echo "<span class='errlabel'>End date is before start date.</span>"; ?> 

                    </div>
                </div>
                <!-- Current Date Button -->
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="button" class="btn btn-default" onclick="useCurrentDate()" value="Use Today's Date">
                    </div>
                </div>
                <!-- Submit Button and Return to Admin Home -->
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" class="btn btn-default" value="Submit" name="submit">
                        <input type="button" class="btn btn-default" onclick="window.location.href='AdminHome.php'" value="Back to Admin Home Page">
                    </div>
                </div>
            </form>
            
            <div id="contentArea"></div>

            <!-- Code for Bar Chart -->
            <div class="col-xs-12 col-lg-6" id="chartArea">  
                <h3>Feedback Levels Over Time: <h3>
        
                <svg width='600' height='300'></svg>
                <script>
                                
                    var svg = d3.select('svg'),
                        margin = {top: 20, right: 20, bottom: 30, left: 40},
                        width = +svg.attr('width') - margin.left - margin.right,
                        height = +svg.attr('height') - margin.top - margin.bottom;
                    
                    var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
                        y = d3.scaleLinear().rangeRound([height, 0]);
                        console.log((3));

                    var g = svg.append("g")
                        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
                    
                    d3.json('getAppData.php', function(error, data){
                        if(error) throw error;
                        
                        data.forEach(function(d){
                        d.letter = d.day;
                        d.frequency = +d.inputs;
                        })
                        
                        console.log(data);
                        
                        if (error) throw error;
                        
                        x.domain(data.map(function(d) { return d.letter; }));
                        y.domain([0, d3.max(data, function(d) { return d.frequency; })]);
                        
                        g.append('g')
                            .attr('class', 'axis axis--x')
                            .attr('transform', 'translate(0,' + height + ')')
                            .call(d3.axisBottom(x));
                        
                        g.append('g')
                            .attr('class', 'axis axis--y')
                            .call(d3.axisLeft(y).ticks(4, 's'))
                        .append('text')
                            .attr('transform', 'rotate(-90)')
                            .attr('y', 6)
                            .attr('dy', '0.71em')
                            .attr('text-anchor', 'end')
                            .text('Frequency');
                        
                        g.selectAll('.bar')
                        .data(data)
                        .enter().append('rect')
                            .attr('class', 'bar')
                            .attr('x', function(d) { return x(d.letter); })
                            .attr('y', function(d) { return y(d.frequency); })
                            .attr('width', x.bandwidth())
                            .attr('height', function(d) { return height - y(d.frequency); });

                        // text label for the x axis
                        g.append("text")             
                            .attr("transform",
                                    "translate(" + (width/2) + " ," + 
                                                (height + margin.top + 10) + ")")
                            .style("text-anchor", "middle")
                            .style("font-size", "14")
                            .text("Date");

                        // text label for the y axis
                        g.append("text")
                            .attr("transform", "rotate(-90)")
                            .attr("y", -4 - margin.left)
                            .attr("x",0 - (height / 2))
                            .attr("dy", "1em")
                            .style("text-anchor", "middle")
                            .style("font-size", "14")
                            .text("Number of Inputs"); 
                                });

                </script>
            </div>    
    </div>
</body>
</html>
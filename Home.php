<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home Page</title>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="homePage.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</head>

<body>
    <div class="container-fluid">
        <div id="nav-placeholder">

        </div>

        <script>
            $(function() {
                $("#nav-placeholder").load("nav.php");
            });
        </script>
        <!-- carousel -->
        <div id="carousel1" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel1" data-slide-to="0" class="active"></li>
                <li data-target="#carousel1" data-slide-to="1"></li>
                <li data-target="#carousel1" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img class="caroImg" src="newman2.png">
                    <div class="carousel-caption">
                        <h2>Let your classmates know how busy Newman Library is!</h2>
                    </div>
                </div>
                <div class="item">
                    <img class="caroImg" src="rewards1.jpg">
                    <div class="carousel-caption">
                        <h2>Don't feel like sharing your information? Maybe these rewards might convince you!</h2>
                    </div>
                </div>
                <div class="item">
                    <img class="caroImg" src="team.jpg">
                    <div class="carousel-caption">
                        <h2>Website and idea created by three Senior Virginia Tech BIT students!</h2>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev" id="navigators">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel1" role="button" data-slide="next" id="navigators">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Welcome to "Spaced Out"!</h1>
            <p class="lead">Have you ever went to Newman Library only to get there and see it's full? </p>
            <hr class="my-4">
            <p>Well now you can use this site before you even leave your house to see if Newman is full!</p>
        </div>
    </div>

    <div class="container-fluid text-center">
        <div class="row content">
            <div class="col-sm-2 sidenav">
                <p><a href="input.php">Input Information</a></p>
                <p><a href="viewLocations.php">View Location</a></p>
                <p><a href="about.html">About</a></p>
            </div>
            <div class="col-sm-8 text-left">
                <h1>Welcome <?php session_start();
                            if (isset($_SESSION['fname'])) {
                                echo $_SESSION['fname'];
                            }
                            ?>!</h1>
                <p class="welcomeText">
                    <br><BR></BR>
                    Hello! Use the navigation bar at the top of the page or the links to the left to access other pages on this site.
                </p>
            </div>
        </div>
    </div>

</body>


</html>
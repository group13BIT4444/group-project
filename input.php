<?php

use Phpml\Preprocessing\NumberConverter;

date_default_timezone_set("America/New_York");

$tLevel = 0;
$location = 0;
$level = 0;
$locationID = 0;
$err = false;
$studentid = 0;
$loggegdIn = false;
$pointBalance = 0;
$currentBalance = 0;
$accountid = 0;

// checks to see if each portion of the form was filled out prior to submission
if (isset($_POST["submit"])) {
  if (isset($_POST["trafficLevel"])) $tLevel = $_POST["trafficLevel"];
  if (isset($_POST["location"])) $locationID = $_POST["location"];
  if (isset($_POST["level"])) $level = $_POST["level"];
  session_start();
  if (isset($_SESSION['studentid'])) {
    $loggedIn = true;
    $studentid = $_SESSION['studentid'];
    $accountid = $_SESSION['account_id'];
  }

  if (!$err) {
    require_once("db.php");
    $sql = "INSERT into input (student_id, location_id, trafficLevel, datetimeStamp) VALUES ($studentid, '$locationID', '$tLevel', now())";
    $result = $mydb->query($sql);
    $currentBalance = $_SESSION['point_balance'];

    $pointBalance = $currentBalance + 1;
    $sql = "UPDATE student SET point_balance = $pointBalance WHERE student_id = $studentid";
    $_SESSION['point_balance'] = $pointBalance;
    $result = $mydb->query($sql);



    if ($loggedIn) {
      $_SESSION['point_balance'] = $pointBalance;
      Header("HTTP/1.1 307 Temprary Redirect");
      Header("Location: Home.php");
    } elseif (!$loggedIn) {
      Header("HTTP/1.1 307 Temprary Redirect");
      Header("Location: Home.php");;
    }
  } else {
    $err = true;
  }
}

?>

<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Input Information</title>
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <meta charset="utf-8">
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>


<body>
  <div class="content fluid">
    <!-- placeholder for the navigation bar at the top of the page -->
    <div id="nav-placeholder">

    </div>

    <script>
      $(function() {
        $("#nav-placeholder").load("nav.php");
      });
    </script>


    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
      <div class="form-row">
        <div class="form-group col-md-4">
          <!-- A list box with each location and appended floor from the spacedout database -->
          <label for="location">Which location are you at right now?</label>
          <select id="location" class="form-control" name="location">
            <option disabled selected>Location:</option>
            <?php
            require_once("db.php");
            $sql = "SELECT location_name, location_id, floor FROM location";
            $result = $mydb->query($sql);

            while ($row = mysqli_fetch_array($result)) {
              echo "<option value=" . $row["location_id"] . ">" . $row["location_name"] . " Floor " . $row["floor"] . "</option>";
            }
            ?>
          </select>
          <br>
          <div class="form-group col-md-4">
            <!-- List box with pre-populated options for the traffic level of a location -->
            <label for="trafficLevel">How busy is your location right now?</label>
            <select aria-placeholder="Enter traffic level" id="trafficLevel" class="form-control" name="trafficLevel">
              <option disabled selected>Enter traffic level:</option>
              <option value=3>Packed</option>
              <option value=2>Busy</option>
              <option value=1>Not Busy</option>
            </select>
          </div>
          <br>
        </div>
        <input type="submit" name="submit" value="Submit" class="submit"></input>
    </form>
  </div>
</body>

</html>
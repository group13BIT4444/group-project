<?php
$user = "";
$pwd = "";
$fname = "";
$lname = "";
$accountid = 0;
$isAdmin = 0;
$err = false;
$loginOK = null;
$studentid = 0;
$pointBalance = 0;

if (isset($_POST["submit"])) {
  if (isset($_POST["uname"])) $user = $_POST["uname"];
  if (isset($_POST["pwd"])) $pwd = $_POST["pwd"];

  if (empty($user) || empty($pwd)) {
    $err = true;
  }

  if (!$err) {
    require_once("db.php");
    $sql = "SELECT login.account_id, uname, pwd, fname, lname, isAdmin from login
    WHERE uname='$user' and pwd='$pwd'";
    $result = $mydb->query($sql);

    $row = mysqli_fetch_array($result);

    if ($row) {
      if (strcmp($user, $row["uname"]) == 0 && strcmp($pwd, $row["pwd"]) == 0) {
        session_start();
        $fname = $row["fname"];
        $lname = $row["lname"];
        $loginOK = true;
        $user = $row['uname'];
        $pwd = $row['pwd'];
        $isAdmin = $row['isAdmin'];
        $accountid = $row['account_id'];

        $sql = "SELECT student_id, point_balance FROM student 
          WHERE student.account_id = $accountid";
        $result = $mydb->query($sql);
        $row = mysqli_fetch_array($result);
        $studentid = $row['student_id'];
        $pointBalance = $row['point_balance'];
        $_SESSION['point_balance'] = $pointBalance;
        $_SESSION['studentid'] = $studentid;
      }
    } else {
      $loginOK = false;
      $err = true;
    }

    if ($loginOK) {
      $_SESSION['uname'] = $user;
      $_SESSION["pwd"] = $pwd;
      $_SESSION['fname'] = $fname;
      $_SESSION["lname"] = $lname;
      $_SESSION["isAdmin"] = $isAdmin;
      $_SESSION["point_balance"] = $pointBalance;
      $_SESSION["account_id"] = $accountid;
      Header("HTTP/1.1 307 Temprary Redirect");
      Header("Location: Home.php");
    } elseif (!$loginOK) {
      $err = true;
    }
  } else {
    $err = true;
  }
}


?>

<!DOCTYPE html>

<html>

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <meta charset="utf-8">
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link href="loginPage.css" rel="stylesheet" />


</head>

<body>
  <div id="nav-placeholder">

  </div>

  <script>
    $(function() {
      $("#nav-placeholder").load("nav.php");
    });
  </script>

  <div align="center" class="loginBox">
    <div style="width:300px; border: solid 1px #333333; " align="left">
      <div style="background-color:#333333; color:#FFFFFF; padding:3px;"><b>Login</b></div>
      <div style="margin:30px">

        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
          <label>Username :
            <input type="text" name="uname" class="box" placeholder="Please enter a username" />
          </label>
          <?php
          if ($err) {
            echo "<br><label class='errlabel'>Error: Please enter a valid username.</label>";
          }
          ?>
          <br /><br />
          <label>Password :
            <input type="password" name="pwd" class="box" placeholder="Please enter a password" /></label>
          <?php
          if ($err) {
            echo "<br><label class='errlabel'>Error: Please enter a valid password.</label>";
          }
          ?>
          <br />
          <br />
          <input type="submit" name="submit" value="Submit" /><br />
        </form>
      </div>
    </div>
  </div>
  <br>
  <a href="createAccount.php">
    <div align="center">Click here to create an account</div>
  </a>
</body>

</html>
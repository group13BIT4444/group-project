<!doctype html>

<html>
    <head>
        <title>Availability Estimate</title>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="homePage.css" rel="stylesheet" />
        <script src="jquery-3.1.1.min.js"></script>
        <script src='https://d3js.org/d3.v4.min.js'></script>

        <style>
            
        </style>
    </head>

    <body>
        <!--Load the navbar and it's CSS-->
        <div class="container-fluid">
            <div id="nav-placeholder">

            </div>

            <script>
                $(function() {
                    $("#nav-placeholder").load("nav.php");
                });
            </script>
        </div>

        <div class="col-sm-12 sidenav text-center">
            <?php
                require_once("db.php");
                $selectTime = "";
                $location = 0;
                $startTime = "";
                $endTime = "";
                $totalTraffic = 0;
                $inputCount = 1;
                $avgTraffic = 0;

                if (isset($_GET["selectTime"])) $selectTime = $_GET["selectTime"];
                if (isset($_GET["location"])) $location = $_GET["location"];

                if ($selectTime >= "00:00" and $selectTime < "01:00"){
                    $startTime = "00:00";
                    $endTime = "01:00";
                } elseif ($selectTime >= "01:00" and $selectTime < "02:00") {
                    $startTime = "01:00";
                    $endTime = "02:00";
                } elseif ($selectTime >= "02:00" and $selectTime < "03:00") {
                    $startTime = "02:00";
                    $endTime = "03:00";
                } elseif ($selectTime >= "03:00" and $selectTime < "04:00") {
                    $startTime = "03:00";
                    $endTime = "04:00";
                } elseif ($selectTime >= "04:00" and $selectTime < "05:00") {
                    $startTime = "04:00";
                    $endTime = "05:00";
                } elseif ($selectTime >= "05:00" and $selectTime < "06:00") {
                    $startTime = "05:00";
                    $endTime = "06:00";
                } elseif ($selectTime >= "06:00" and $selectTime < "07:00") {
                    $startTime = "06:00";
                    $endTime = "07:00";
                } elseif ($selectTime >= "07:00" and $selectTime < "08:00") {
                    $startTime = "07:00";
                    $endTime = "08:00";
                } elseif ($selectTime >= "08:00" and $selectTime < "09:00") {
                    $startTime = "08:00";
                    $endTime = "09:00";
                } elseif ($selectTime >= "09:00" and $selectTime < "10:00") {
                    $startTime = "09:00";
                    $endTime = "10:00";
                } elseif ($selectTime >= "10:00" and $selectTime < "11:00") {
                    $startTime = "10:00";
                    $endTime = "11:00";
                } elseif ($selectTime >= "11:00" and $selectTime < "12:00") {
                    $startTime = "11:00";
                    $endTime = "12:00";
                } elseif ($selectTime >= "12:00" and $selectTime < "13:00") {
                    $startTime = "12:00";
                    $endTime = "13:00";
                } elseif ($selectTime >= "13:00" and $selectTime < "14:00") {
                    $startTime = "13:00";
                    $endTime = "14:00";
                } elseif ($selectTime >= "14:00" and $selectTime < "15:00") {
                    $startTime = "14:00";
                    $endTime = "15:00";
                } elseif ($selectTime >= "15:00" and $selectTime < "16:00") {
                    $startTime = "15:00";
                    $endTime = "16:00";
                } elseif ($selectTime >= "16:00" and $selectTime < "17:00") {
                    $startTime = "16:00";
                    $endTime = "17:00";
                } elseif ($selectTime >= "17:00" and $selectTime < "18:00") {
                    $startTime = "17:00";
                    $endTime = "18:00";
                } elseif ($selectTime >= "18:00" and $selectTime < "19:00") {
                    $startTime = "18:00";
                    $endTime = "19:00";
                } elseif ($selectTime >= "19:00" and $selectTime < "20:00") {
                    $startTime = "19:00";
                    $endTime = "20:00";
                } elseif ($selectTime >= "20:00" and $selectTime < "21:00") {
                    $startTime = "20:00";
                    $endTime = "21:00";
                } elseif ($selectTime >= "21:00" and $selectTime < "22:00") {
                    $startTime = "21:00";
                    $endTime = "22:00";
                } elseif ($selectTime >= "22:00" and $selectTime < "23:00") {
                    $startTime = "22:00";
                    $endTime = "23:00";
                } elseif ($selectTime >= "23:00" and $selectTime <= "23:59") {
                    $startTime = "23:00";
                    $endTime = "23:59";
                }
                $sql = "SELECT location_id, trafficLevel, cast(datetimeStamp as time) FROM `input` WHERE location_id=".$location." and cast(datetimeStamp as time) > '".$startTime."' and cast(datetimeStamp as time) < '".$endTime."'";
                $result = $mydb->query($sql);
                while ($row=mysqli_fetch_array($result)){
                    $totalTraffic = $totalTraffic + $row["trafficLevel"];
                    ++$inputCount;
                }
                $avgTraffic = $totalTraffic / $inputCount;
                if ($avgTraffic >= 0 and $avgTraffic <= 1.5) {
                    echo "<p>This location at ".$selectTime." should not be busy</p><br>";
                } elseif ($avgTraffic >= 1.5 and $avgTraffic <= 2) {
                    echo "<p>This location at ".$selectTime." is likely somewhat busy</p><br>";
                } elseif ($avgTraffic >= 2) {
                    echo "<p>This location at ".$selectTime." is likely busy</p><br>";
                };
            ?>
            <input type="button" onclick="location.href='availEstimate.php'" value="Reselect Location and Time">
        </div>
    </body>
</html>
<!doctype html>

<html>
    <head>
        <title>Compare Spaces</title>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="homePage.css" rel="stylesheet" />
        <script src="jquery-3.1.1.min.js"></script>
        <script src="https://d3js.org/d3.v4.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <style>
            option, p, select{
                font-size: 16px;
            }
            img {
                display: block;
                width: 1000px;
                height: 800px;
                margin-left: auto;
                margin-right: auto;
            }
        </style>

        <script type="text/javascript">
            //event handler that executes the init function once all html elements are loaded
            document.addEventListener("DOMContentLoaded", init);
            
		    function getContent() {
                
                //get content from the server using XMLHttpRequest
                var asyncRequest;
                try {
                    asyncRequest = new XMLHttpRequest();

                    var url="displayMap.php";
                    asyncRequest.onreadystatechange = stateChange;
                    asyncRequest.open('GET', url, true);
                    asyncRequest.send(null);
                } catch (exception) {alert(exception);}
            
                function stateChange(){
                    //function executed if request status changes to completed
                    if (asyncRequest.readyState==4 && asyncRequest.status==200) {
                        document.getElementById("contentArea").innerHTML = asyncRequest.responseText;
                    }
                }
            }
            function getContent2() {
                
                //get content from the server using XMLHttpRequest
                var asyncRequest;
                try {
                    asyncRequest = new XMLHttpRequest();

                    var url="displayMap.php";
                    asyncRequest.onreadystatechange = stateChange;
                    asyncRequest.open('GET', url, true);
                    asyncRequest.send(null);
                } catch (exception) {alert(exception);}
            
                function stateChange(){
                    //function executed if request status changes to completed
                    if (asyncRequest.readyState==4 && asyncRequest.status==200) {
                        document.getElementById("contentArea2").innerHTML = asyncRequest.responseText;
                    }
                }
            }

            //initialization function 
            function init(){
                var locationSelect = document.getElementById("location");
                locationSelect.addEventListener("change", getContent);
                var locationSelect2 = document.getElementById("location2");
                locationSelect2.addEventListener("change", getContent2);
		    }
		    
            //jquery that displays the map of the user's selection
            function locSelect(){
                $("#location").change(function(){
                    var location_id = document.getElementById("location").value;
                    $.ajax({
                        url: "displayMap.php?location_id="+location_id,
                        async: true,
                        success: function(result) {
                            $("#contentArea").html(result);
                        }
                    })
                });
            }
            function locSelect2(){
                $("#location2").change(function(){
                    var location_id = document.getElementById("location2").value;
                    $.ajax({
                        url: "displayMap.php?location_id="+location_id,
                        async: true,
                        success: function(result) {
                            $("#contentArea2").html(result);
                        }
                    })
                });
            }
        </script>
    </head>

    <body>
        <!--Load the navbar and it's CSS-->
        <div class="container-fluid">
            <div id="nav-placeholder">

            </div>

            <script>
                $(function() {
                    $("#nav-placeholder").load("nav.php");
                });
            </script>
        </div>

        <!--Two div elements that essentially do the same thing-->
        <!--Will show a location and how busy it currently is-->
        <!--Two div elements lets the user see two different locations at the same time-->
        <div class="container-fluid">
            <div class="col-sm-6 sidenav text-center">
            <form method="post">
                <!--<p>Currently Viewing: "location"</p>-->
                    <p>Change Location? </p>
                    <select id="location" onchange="locSelect()">
                        <?php
                            require_once("db.php");
                            $sql = "select * from location";
                            $result = $mydb->query($sql);
                            while ($row=mysqli_fetch_array($result)){
                                echo "<option id=".$row["location_name"]." value=".$row["location_id"].">".$row["location_name"]." Floor ".$row["floor"]."</option>";
                            }
                        ?>
                    </select>
            </form>
                <div class="col-sm-6 sidenav text-center" id="contentArea">
                    &nbsp;
                </div>
            </div>

            <div class="col-sm-6 sidenav text-center">
            <form method="post">
                <!--<p>Currently Viewing: "location"</p>-->
                <p>Change Location? </p>
                <select id="location2" onchange="locSelect2()">
                    <?php
                        require_once("db.php");
                        $sql = "select * from location";
                        $result = $mydb->query($sql);
                        while ($row=mysqli_fetch_array($result)){
                            echo "<option id=".$row["location_name"]." value=".$row["location_id"].">".$row["location_name"]." Floor ".$row["floor"]."</option>";
                        }
                    ?>
                </select>
            </form>
                <div class="col-sm-12 sidenav text-center" id="contentArea2">
                    &nbsp;
                </div>
            </div>
        </div>
    </body>

</html>
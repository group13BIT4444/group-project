<!doctype html>

<html>
    <head>
        <title>View Locations</title>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="homePage.css" rel="stylesheet" />
        <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
        <style>
            option, p, select{
                font-size: 16px;
            }
            img {
                display: block;
                width: 1400px;
                height: 1000px;
                margin-left: auto;
                margin-right: auto;
            }
        </style>
        <script type="text/javascript">
            //event handler that executes the init function once all html elements are loaded
            document.addEventListener("DOMContentLoaded", init);
            
		    function getContent() {
                
                //get content from the server using XMLHttpRequest
                var asyncRequest;
                try {
                    asyncRequest = new XMLHttpRequest();

                    var url="displayMap.php";
                    asyncRequest.onreadystatechange = stateChange;
                    asyncRequest.open('GET', url, true);
                    asyncRequest.send(null);
                } catch (exception) {alert(exception);}
            
                function stateChange(){
                    //function executed if request status changes to completed
                    if (asyncRequest.readyState==4 && asyncRequest.status==200) {
                        document.getElementById("contentArea").innerHTML = asyncRequest.responseText;
                    }
                }
            }

            //initialization function 
            function init(){
                var locationSelect = document.getElementById("location");
                locationSelect.addEventListener("change", getContent);
		    }
		    
            //jquery that displays the map of the user's selection
            function locSelect(){
                $("#location").change(function(){
                    var location_id = document.getElementById("location").value;   
                    $.ajax({
                        url: "displayMap.php?location_id="+location_id,
                        async: true,
                        success: function(result) {
                            $("#contentArea").html(result);
                        }
                    })
                });
            }
        </script>
    </head>

    <body>
        <!--Load the navbar and it's CSS-->
        <div class="container-fluid">
            <div id="nav-placeholder">

            </div>

            <script>
                $(function() {
                    $("#nav-placeholder").load("nav.php");
                });
            </script>
        </div>

        <div class="col-sm-12 sidenav text-center">
            <form method="post">
                <p>Change Location? </p>
                <select id="location" onchange="locSelect()">
                    <option value="" selected disabled>Select a Location</option>
                    <?php
                        require_once("db.php");
                        $sql = "select * from location";
                        $result = $mydb->query($sql);
                        while ($row=mysqli_fetch_array($result)){
                            echo "<option id=".$row["location_name"]." value=".$row["location_id"].">".$row["location_name"]." Floor ".$row["floor"]."</option>";
                        }
                    ?>
                </select> <br> <br>
            </form>

            <div id="contentArea">
                &nbsp;
            </div>
        </div>
    </body>

</html>
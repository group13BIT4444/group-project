<!--Processes Form -->
<?php
    //initialize variables
        $fname="";
        $lname="";
        $username ="";
        $pwd ="";
        $confirmpwd="";
        $err = false;
        $erruname = false;
    
    //sets variables
    if(isset($_POST['submit'])){
        if(isset($_POST['fname'])) $fname = $_POST['fname'];
        if(isset($_POST['lname'])) $lname = $_POST['lname'];
        if(isset($_POST['uname'])) $username = $_POST['uname'];
        if(isset($_POST['pwd'])) $pwd = $_POST['pwd'];
        if(isset($_POST['confirmpwd'])) $confirmpwd = $_POST['confirmpwd'];

        if(empty($fname) || empty($lname) || empty($pwd)){
            $err = true;
        }
        if(!empty($fname) && !empty($lname) && !empty($username) && !empty($pwd) && !empty($confirmpwd)){

            require_once("db.php");
            $sql = "SELECT * FROM login WHERE uname='$username'";
            $result = $mydb->query($sql);

            if (mysqli_num_rows($result) > 0) { //check to see if username already exists
                $err=true;
                $erruname = true;
            }elseif($pwd != $confirmpwd){ //check to see if passwords match
                $err=true;
            }else{ //if no errors insert into the login table
                $sql = "INSERT INTO spacedout.login (fname, lname, uname, pwd, isAdmin)
                    VALUES ('$fname','$lname','$username', '$pwd', 0)";
                $result = $mydb->query($sql);
                $sql = "SELECT * FROM spacedout.login WHERE uname = '$username' AND pwd = '$pwd' AND fname = '$fname' AND lname = '$lname'";
                $result = $mydb->query($sql);
                $row = mysqli_fetch_array($result);
                //insert into student table
                $sql = "INSERT INTO spacedout.student (student_id, point_balance, account_id) VALUES (DEFAULT, DEFAULT, ".$row['account_id'].")";
                $result = $mydb->query($sql);
            }
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create New Account</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <!-- Error Label styling -->
    <style>
        .errlabel {color:red};
    </style>


</head>

<body>
    <div class="container-fluid">
        <!-- Nav Bar Code -->
        <div id="nav-placeholder">
            </div>

            <script>
                $(function() {
                $("#nav-placeholder").load("nav.php");
                });
        </script>
        
        <!-- code for form -->
        <form class="form-horizontal"  method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <!-- First Name Input -->
            <div class="form-group">
                <label class="control-label col-sm-2" for="fname">First Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="fname" placeholder="Enter first name" value="<?php echo $fname; ?>">
                    <?php if($err && empty($fname)) echo "<span class='errlabel'>Please enter your first name</span>"; ?>

                </div>
            </div>
            
            <!-- Last Name Input -->
            <div class="form-group">
                <label class="control-label col-sm-2" for="lname">Last Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="lname" placeholder="Enter last name" value="<?php echo $lname; ?>">
                    <?php if($err && empty($lname)) echo "<span class='errlabel'>Please enter your last name</span>"; ?>

                </div>
            </div>

            <!-- Username Input -->
            <div class="form-group">
                <label class="control-label col-sm-2" for="uname">Create a Username:</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" name="uname" placeholder="Enter username" value="<?php echo $username; ?>">
                    <?php if($err && empty($username)) echo "<span class='errlabel'>Please enter a username</span>"; ?>
                    <?php if($err && $erruname) echo "<span class='errlabel'>That username is already taken</span>"; ?>

                </div>
            </div>

            <!-- Password Input -->
            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd1">Create a Password:</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" name="pwd" placeholder="Enter password" value="<?php echo $pwd; ?>">
                    <?php if($err && empty($pwd)) echo "<span class='errlabel'>Please enter a password</span>"; ?>

                </div>
            </div>

            <!-- Confirm Password -->
            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Re-Enter your Password:</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" name="confirmpwd" placeholder="Re-enter password" value="<?php echo $confirmpwd; ?>">
                    <?php if($err && $pwd != $confirmpwd) echo "<span class='errlabel'>Passwords do not match</span>"; ?>

                </div>
            </div>

            <!-- Submit Button -->
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" class="btn btn-default" name="submit" value="Submit">
                    <?php if(!$err) echo "<span class='errLabel'>Account Created!</span>"; ?>
                </div>
            </div>
            <br>
            <p>Already have an account? <a href="login.php">Login here</a>.</p>
            <div id="accountcreated"></div>
        </form>    
    </div>
</html>



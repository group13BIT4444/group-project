<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <title>Location Analysis</title>
</head>
<!-- Code for Use Current Date button -->
<script> 
   function useCurrentDate(){ 
       var today = new Date();
       var month = today.getMonth()+1;
       var day = today.getDay()+1;
       if(month.toString().length<2){
           month = "0" + month;
           Number(month);
       }
       if(day.toString().length<2){
           day = "0" + day;
           Number(day);
       }
       var year = today.getFullYear();
        document.getElementById("enddate").value = year+"-"+month+"-"+day;
    }
</script>

<!-- Submit Button Ajax -->
<script> 
    function getContent(){
        var asyncRequest;
        var z = document.getElementById("contentArea");
        var startdate = document.forms[0].startdate.value;
        var enddate = document.forms[0].enddate.value;
        var location = document.forms[0].locatoin.value;
        var url="LocationAnalysisSubmit.php?startDate="+startdate&"endDate="+enddate&"location"&location;

        try {
          asyncRequest = new XMLHttpRequest();  //create request object
          asyncRequest.onreadystatechange=stateChange;
          asyncRequest.open('GET',url,true);  // prepare the request
          asyncRequest.send(null);  // send the request
        }
        catch(exception) {alert("Request failed");
        }
 
		function stateChange() {
			if(asyncRequest.readyState==4 && asyncRequest.status==200) {
				document.getElementById("contentArea").innerHTML=
					asyncRequest.responseText;
			}
      }
    }
</script>

<body>
    
    <div class="container-fluid">
    <!-- Nav Bar Code -->
    <div id="nav-placeholder">
        </div>
        <script>
        $(function() {
        $("#nav-placeholder").load("nav.php");
            });
    </script>
    <!-- code for page content -->
    <div class="container-fluid text-center">
        <div class="row content">
            <!-- code for side bar page links -->
            <div class="col-sm-2 sidenav">
                <p><a href="AdminHome.php">Admin Home Page</a></p>
                <p><a href="AppUsageAnalysis.php">App Usage Analysis</a></p>
                <p><a href="ManageApp.php">Manage Application</a></p>
            </div>
            <!-- code for Welcome paragraph -->
            <div class="col-sm-8 text-left">
                <h2>Welcome to the Location Analysis Page!</h2>
                <p>Please select a time frame and a location you would like to get information on.</p>
                <hr>
            </div>
        </div>
    </div>

    
        
        <!-- form code -->
        <form class="form-horizontal" method="get" action="LocationAnalysisSubmit.php">
            <!-- start date code -->
            <div class="form-group">
                <label class="control-label col-sm-2" for="startDate">Start Date:</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="startdate">
                </div>
            </div>

            <!-- End date code -->
            <div class="form-group">
                <label class="control-label col-sm-2" for="enddate">End Date:</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="enddate" id="enddate">
                </div>
            </div>

            <!-- Use Current Date button -->
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <input type="button" class="btn btn-default" onclick="useCurrentDate()" value="Use Today's Date">
                </div>
            </div>

            <!-- Location Select -->
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <select class="" name="location" id="location">
                <option>Select a location</option>
                    <?php
                        $array = [];
                        require_once("db.php");
                            $sql = "SELECT * FROM LOCATION";
                            $result = $mydb->query($sql);
                            while($row = mysqli_fetch_array($result)){
                                echo "<option value=".$row['location_id'].">".$row['location_name']." ".$row["floor"]."</option>";
                            }
                    ?>
                </select>
                </div>
            </div>    
            
            <!-- Submit and Back To Home button -->
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" name="submit" class="btn btn-default" value="Submit">
                    <input type="button" class="btn btn-default" onclick="window.location.href='AdminHome.php'" value="Back to Admin Home Page">

                </div>
        </form>
        
        <div id="contentArea" name = "contentArea"></div>

    </div>
</body>
</html>
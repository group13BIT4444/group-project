<?php 

echo "<head>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Location Analysis</title>
    <link href='css/bootstrap.min.css' rel='stylesheet' />
    <meta charset='utf-8'>
    <script src='jquery-3.1.1.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    </head>
    <div class='container-fluid'>
    <!-- Nav Bar Code -->
    <div id='nav-placeholder'>
        </div>
        <script>
        $(function() {
        $('#nav-placeholder').load('nav.php');
            });
    </script>
    <div class='container-fluid text-center'>
        <div class='row content'>
            <!-- code for side bar page links -->
            <div class='col-sm-2 sidenav'>
                <p><a href='AdminHome.php'>Admin Home Page</a></p>
                <p><a href='AppUsageAnalysis.php'>App Usage Analysis</a></p>
                <p><a href='ManageApp.php'>Manage Application</a></p>
            </div>
            <!-- code for Welcome paragraph -->
            <div class='col-sm-8 text-left'>
                <h2>Welcome to the Location Analysis page!</h2>
                <p>Please select a time frame you would like to get information on.</p>
                <hr>
            </div>
        </div>
    </div>

"; 

    require_once("db.php");

    //defaults to today, how to default to earlier
    $startDate = date("m-d-y", time());
    $endDate = date("m-d-y", time());
    $locationid = 0;
    $imgsrc = "";

    
    if(isset($_GET["startdate"])) $startDate=$_GET["startdate"];
    if (isset($_GET["enddate"])) $endDate = $_GET["enddate"];
    if (isset($_GET["location"])) $locationid = $_GET["location"];


    /*Grabs Location Title */
    $sql = "SELECT * FROM LOCATION WHERE location_id=".$locationid;
        $result = $mydb->query($sql);
        $row = mysqli_fetch_array($result);

        echo "<h4>You are currently viewing: ".$row['location_name']." - Floor ".$row['floor']." from ".$startDate." through ".$endDate.".</h4>";
        echo "<img src=".$imgsrc.">";

    /* Displays selected locations average traffic level */
    $sql = "SELECT ROUND(AVG(TrafficLevel),0) AS avgTraffic, location_name AS locname, floor FROM INPUT
        INNER JOIN spacedout.location ON spacedout.location.Location_ID = input.location_id
        WHERE input.location_id = $locationid";// AND input.datetimeStamp >= $startDate  AND input.datetimeStamp <= $endDate"; //start and end date formats aren't compatible with sql format
        $result = $mydb->query($sql);
        $row=mysqli_fetch_array($result);
            if(empty($row['avgTraffic'])){
                echo "<p>".$row['locname']." - Floor ".$row['floor']."'s traffic level has not been rated yet! Ask users to send more feedback.</p>";
            }else{
                echo "<p>".$row['locname']." - Floor ".$row['floor']."'s traffic level was rated ".$row['avgTraffic']."/3 on average.</p>";
                echo "<div class='progress'>";
                $percent = $row['avgTraffic'] *33.33333;
                echo "<div class='progress-bar bg-info' role='progressbar' style='width: ".$percent."%;' aria-valuenow=".$row['avgTraffic']." aria-valuemin='0'
                        aria-valuemax='3'>".$row['avgTraffic']."</div></div>";        
            }

    /* Amount of feedback on location */
    $sql = "SELECT COUNT(DISTINCT Input_ID) AS 'numInput' FROM INPUT     
        INNER JOIN spacedout.location ON spacedout.location.Location_ID = input.location_id
        WHERE input.location_id = $locationid"; // need to add date code to 

        $result = $mydb->query($sql);
        while($row=mysqli_fetch_array($result)){
            echo "<p>People gave feedback ".$row['numInput']." time(s) on this location.</p>";
        }

     /*Locations listed by Average Traffic Level */
     echo "<br>";
        echo "<h4>Total Locations Listed By Average Traffic Level:</h4>";
        echo "<ol>";
        $sql = "SELECT location_name, floor, ROUND(AVG(TrafficLevel)) As avgTraffic FROM INPUT
            INNER JOIN spacedout.location ON spacedout.location.Location_ID = input.location_id
            GROUP BY input.location_id ORDER BY TrafficLevel DESC";
        $result = $mydb->query($sql);
        while ($row=mysqli_fetch_array($result)){
            echo "<li>".$row['location_name']." - Floor ".$row['floor']." - Average Traffic Level: ".$row['avgTraffic']."</li>";
        }
        echo "</ol>";

    /*Locations listed by amount of feedback*/
    echo "<br>";
        echo "<h4>Total Locations Listed By Amount Of Feedback</h4>";
        echo "<ol>";
        $sql = "SELECT location_name, floor, COUNT(Input_ID) As numInput FROM INPUT
            INNER JOIN spacedout.location ON spacedout.location.Location_ID = input.location_id
            GROUP BY input.location_id ORDER BY numInput DESC";
        $result = $mydb->query($sql);
        while ($row=mysqli_fetch_array($result)){
            echo "<li>".$row['location_name']." - Floor ".$row['floor']." - ".$row['numInput']." inputs received </li>";
        }
        echo "</ol>";
        
    echo "<input type=\"button\" class=\"btn btn-default\" onclick=\"location.href='LocationAnalysis.php'\" value=\"Resubmit Dates\">";

 
?>
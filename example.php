<?php

require_once __DIR__ . '/Simplex/Simplex.php';

$cph1 = 0;
$cph2 = 0;
$cpp1 = 0;
$cpp2 = 0;
$objcoef1 = 0;
$objcoef2 = 0;

if (isset($_POST['submit'])) {
    if (isset($_POST['cph1']) && isset($_POST['cph2'])
    && isset($_POST['cpp1']) && isset($_POST['cpp2'])
    && isset($_POST['objcoef1']) && isset($_POST['objcoef2'])) {
        $cph1 = $_POST['cph1'];
        $cph2 = $_POST['cph2'];
        $cpp1 = $_POST['cpp1'];
        $cpp2 = $_POST['cpp2'];
        $objcoef1 = $_POST['objcoef1'];
        $objcoef2 = $_POST['objcoef2'];
    }
}

//specify objective function
$z = new Simplex\Func(array(
	'x1' => $objcoef1,
	'x2' => $objcoef2,
));

$task = new Simplex\Task($z);

//specify constraints
$task->addRestriction(new Simplex\Restriction(array(
	'x1' => $cph1,
	'x2' => $cph2,

), Simplex\Restriction::TYPE_LOE, 40)); //LOE: less than or equal to


$task->addRestriction(new Simplex\Restriction(array(
	'x1' => $cpp1,
	'x2' => $cpp2,

), Simplex\Restriction::TYPE_LOE, 120)); //GOE: greater than or equal to

$solver = new Simplex\Solver($task);

//Whether an optimal solution exists
$size = sizeof($solver->getSteps());
$hasSolution = $solver->getSteps()[$size-1]->hasSolution();


if ($hasSolution) {
	//optimal objective function value
	$obj=$solver->getSteps()[$size-1]->getZ()->getB()->toFloat();
	//optimal solution
	$x1=$solver->getSteps()[$size-1]->getSolution()['x1']->toFloat();
	$x2=$solver->getSteps()[$size-1]->getSolution()['x2']->toFloat();

	echo "Total profit = ".$obj."<br /> Quantity of Product 1 = ".$x1."; Quantity of Product 2 = ".$x2;
}
else {
	echo "There is no optimal solution";
}

echo "<br> <br>"
?>

<html>

<head>
    <title>Homework 16</title>
</head>

<body>
    <form method="post">
    <label>Cost of labor per product (Product 1)</label>
    <input type="num" name="cph1" value="0.00">
    <br>
    <label>Cost of labor per product (Product 2)</label>
    <input type="num" name="cph2" value="0.00">
    <br>
    <label>Cost of materials per product (Product 1)</label>
    <input type="num" name="cpp1" value="0.00">
    <br>
    <label>Cost of materials per product (Product 2)</label>
    <input type="num" name="cpp2" value="0.00">
    <br>
    <label>Unit price (Product 1)</label>
    <input type="num" name="objcoef1" value="0.00">
    <br>
    <label>Unit price (Product 2)</label>
    <input type="num" name="objcoef2" value="0.00">
    <br>
    <input type="submit" name="submit">
    </form>
</body>

</html>

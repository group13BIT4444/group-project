<!doctype html>
<html>
    <head>
        <Title>Display Map</Title>
        <link href="css/bootstrap.min.css" rel="stylesheet" />
    </head>

    <?php
        $location_id = 0;
        $totalTraffic = 0;
        $inputCount = 1;
        $avgTraffic = 0;
        require_once("db.php");
        if(isset($_GET['location_id'])) $location_id=$_GET['location_id'];

        $sql = "select location_id, trafficLevel, datetimeStamp from input where location_id = ".$location_id." and datetimeStamp - interval 1 hour <= now()";
        $result = $mydb->query($sql);
        while ($row=mysqli_fetch_array($result)){
            $totalTraffic = $totalTraffic + $row["trafficLevel"];
            ++$inputCount;
        }
        $avgTraffic = $totalTraffic / $inputCount;
        if ($avgTraffic >= 0 and $avgTraffic <= 1.5) {
            echo "<p>This location is currently not busy</p><br>";
        } elseif ($avgTraffic >= 1.5 and $avgTraffic <= 2) {
            echo "<p>This location is currently somewhat busy</p><br>";
        } elseif ($avgTraffic >= 2) {
            echo "<p>This location is currently busy</p><br>";
        };

        $sql = "select * from location where location_id = ".$location_id;
        $result = $mydb->query($sql);
        while ($row=mysqli_fetch_array($result)){
            $location_id = $row["location_id"];
        }

        if($location_id == 1){
            echo "<img  src='newmanFloorMaps/newmanFloor1.jpg'>";
        } elseif($location_id == 2){
            echo "<img  src='newmanFloorMaps/newmanFloor2.jpg'>";
        } elseif($location_id == 3){
            echo "<img  src='newmanFloorMaps/newmanFloor3.jpg'>";
        } elseif($location_id == 4){
            echo "<img  src='newmanFloorMaps/newmanFloor4.jpg'>";
        } elseif($location_id == 5){
            echo "<img  src='newmanFloorMaps/newmanFloor5.jpg'>";
        };
    ?>

</html>
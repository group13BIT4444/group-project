-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Dec 12, 2019 at 01:37 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spacedout`
--
CREATE DATABASE IF NOT EXISTS `spacedout` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `spacedout`;

-- --------------------------------------------------------

--
-- Table structure for table `input`
--

DROP TABLE IF EXISTS `input`;
CREATE TABLE IF NOT EXISTS `input` (
  `input_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `trafficlevel` int(11) NOT NULL,
  `datetimeStamp` datetime NOT NULL,
  PRIMARY KEY (`input_id`),
  KEY `student_id` (`student_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4021 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `input`
--

INSERT INTO `input` (`input_id`, `student_id`, `location_id`, `trafficlevel`, `datetimeStamp`) VALUES
(4001, 0, 1, 3, '2019-12-11 14:47:44'),
(4002, 0, 1, 2, '2019-12-11 14:47:48'),
(4003, 0, 3, 1, '2019-12-11 14:47:52'),
(4004, 0, 5, 2, '2019-12-11 14:47:56'),
(4005, 0, 2, 2, '2019-12-11 14:48:12'),
(4006, 0, 4, 2, '2019-12-11 14:48:22'),
(4007, 0, 5, 1, '2019-12-11 16:53:37'),
(4008, 0, 5, 1, '2019-12-11 16:53:40'),
(4009, 0, 5, 1, '2019-12-11 16:53:44'),
(4010, 0, 4, 2, '2019-12-11 18:12:00'),
(4011, 0, 4, 2, '2019-12-11 18:12:02'),
(4012, 0, 4, 2, '2019-12-11 18:12:22'),
(4013, 0, 2, 3, '2019-12-11 18:12:26'),
(4014, 0, 2, 3, '2019-12-11 18:12:30'),
(4015, 0, 3, 1, '2019-12-11 18:12:34'),
(4016, 0, 3, 1, '2019-12-11 18:12:37'),
(4017, 0, 2, 3, '2019-12-11 18:13:50'),
(4018, 0, 2, 3, '2019-12-11 18:13:54'),
(4019, 0, 2, 3, '2019-12-11 18:13:57'),
(4020, 0, 2, 3, '2019-12-11 19:56:23');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(60) NOT NULL,
  `floor` int(11) NOT NULL,
  `location_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `address`, `floor`, `location_name`) VALUES
(1, '560 Drillfield Dr, Blacksburg, VA 24061', 1, 'Newman Library'),
(2, '560 Drillfield Dr, Blacksburg, VA 24061', 2, 'Newman Library'),
(3, '560 Drillfield Dr, Blacksburg, VA 24061', 3, 'Newman Library'),
(4, '560 Drillfield Dr, Blacksburg, VA 24061', 4, 'Newman Library'),
(5, '560 Drillfield Dr, Blacksburg, VA 24061', 5, 'Newman Library');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(15) NOT NULL,
  `pwd` varchar(15) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '1',
  `fname` varchar(20) NOT NULL,
  `lname` varchar(25) NOT NULL,
  `account_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `uname` (`uname`)
) ENGINE=InnoDB AUTO_INCREMENT=2005 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`account_id`, `uname`, `pwd`, `isAdmin`, `fname`, `lname`, `account_created`, `last_login`) VALUES
(2001, 'james', 'murray', 1, 'james', 'murray', '2019-12-11 14:34:50', '2019-12-11 14:34:50'),
(2002, 'bella', 'blankemeyer', 1, 'bella', 'blankemeyer', '2019-12-11 14:34:50', '2019-12-11 14:34:50'),
(2003, 'connor', 'york', 1, 'connor', 'york', '2019-12-11 14:34:50', '2019-12-11 14:34:50'),
(2004, 'alan', 'wang', 0, 'alan', 'wang', '2019-12-11 14:34:50', '2019-12-11 14:34:50');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `point_balance` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`student_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3002 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `point_balance`, `account_id`) VALUES
(3001, 12, 2002);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

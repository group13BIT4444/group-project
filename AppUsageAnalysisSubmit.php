 <?php

        /* Code for top of the page */
        echo "<head>
            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <title>Location Analysis</title>
            <link href='css/bootstrap.min.css' rel='stylesheet' />
            <meta charset='utf-8'>
            <script src='jquery-3.1.1.min.js'></script>
            <script src='js/bootstrap.min.js'></script>
            </head>
            <div class='container-fluid'>
            <!-- Nav Bar Code -->
            <div id='nav-placeholder'>
                </div>
                <script>
                $(function() {
                $('#nav-placeholder').load('nav.php');
                    });
            </script>
            <div class='container-fluid text-center'>
                <div class='row content'>
                    <!-- code for side bar page links -->
                    <div class='col-sm-2 sidenav'>
                        <p><a href='AdminHome.php'>Admin Home Page</a></p>
                        <p><a href='LocationAnalysis.php'>Location Analysis</a></p>
                        <p><a href='ManageApp.php'>Manage Application</a></p>
                    </div>
                    <!-- code for Welcome paragraph -->
                    <div class='col-sm-8 text-left'>
                        <h2>Welcome to the Usage Analysis page!</h2>
                        <p>Please select a time frame you would like to get information on.</p>
                        <hr>
                    </div>
                </div>


        ";


    require_once("db.php");

    //defaults to today, how to default to earlier
    $startDate = date("m-d-y", time());
    $endDate = date("m-d-y", time());

    if(isset($_GET["startdate"])) $startDate=$_GET["startdate"];
    if (isset($_GET["enddate"])) $endDate = $_GET["enddate"];

    
    echo "<div class='col-sm-8 text-left'>";
    echo "</br>";

     //displays list of users by points
     echo "<h4>Total Users Listed By Point Balance:</h4>";
     echo "<ol>";
     $sql = "SELECT fname, lname, point_balance FROM spacedout.login INNER JOIN spacedout.student ON spacedout.student.account_id = spacedout.login.account_id
        GROUP BY student_id ORDER BY point_balance DESC";
    $result = $mydb->query($sql);
    while ($row=mysqli_fetch_array($result)){
        echo "<li>".$row['fname']." ".$row['lname']." - Points: ".$row['point_balance']."</li>";
    }
    echo "</ol>";

    
    //Display the number of active users in the system during selected timeframe
    $sql = "SELECT COUNT(DISTINCT account_id) AS numUser FROM spacedout.login WHERE last_login >= '$startDate' AND last_login <= '$endDate'";
    $result = $mydb->query($sql);
    while($row=mysqli_fetch_array($result)){
        echo "<p>There were ".$row['numUser']." active users during the selected time period.</p>";
    }
    

    //Displays number of times feedback was given during the timeframe
    $sql = "SELECT COUNT(DISTINCT Input_ID) AS 'numInput' FROM spacedout.input WHERE datetimeStamp >= '$startDate' AND datetimeStamp <= '$endDate'";
    $result = $mydb->query($sql);
     while($row=mysqli_fetch_array($result)){
      echo "<p>Users gave feedback ".$row['numInput']." times.</p>";
     }

    //Displays the top user    
    $sql = "SELECT point_balance as points, fname, lname FROM STUDENT INNER JOIN spacedout.login ON spacedout.login.account_id = spacedout.student.account_id
        WHERE point_balance = (
                SELECT MAX(point_balance) 
                    FROM STUDENT
                )";
    $result = $mydb->query($sql);
    while($row=mysqli_fetch_array($result)){
        echo "<p>The top user is ".$row['fname']." ".$row['lname']." with ".$row['points']." points.</p>";
    }

    //could accounts added in the time frame using the account_created timestamp in login tabl

    $sql = "SELECT COUNT(DISTINCT account_id) AS numNew FROM spacedout.login WHERE account_created >= '$startDate' AND account_created <= '$endDate'";
    $result = $mydb->query($sql);
    $row = mysqli_fetch_array($result);
    echo "<p>".$row['numNew']." accounts were created.</p>";
    
    echo "<input type=\"button\" class=\"btn btn-default\" onclick=\"location.href='AppUsageAnalysis.php'\" value=\"Resubmit Dates\">";
    echo  "</div>";

?>

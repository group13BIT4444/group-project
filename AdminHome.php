<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Home Page</title>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Styling -->
    <style>
        h2 {
            font-family: Futura, "Trebuchet MS", Arial, sans-serif;
            font-size: 30px;
            letter-spacing: 1.8px;
            word-spacing: 0.8px;
            color: #FFFFFF;
            text-shadow: 2px 2px 0 #4074b5, 2px -2px 0 #4074b5, -2px 2px 0 #4074b5, -2px -2px 0 #4074b5, 2px 0px 0 #4074b5, 0px 2px 0 #4074b5,
                -2px 0px 0 #4074b5, 0px -2px 0 #4074b5;
            color: #FFFFFF;
            font-weight: 700;
            text-decoration: none solid rgb(68, 68, 68);
            font-style: normal;
            font-variant: normal;
            text-transform: uppercase;
        }

        .navigators {
            color: red;
        }

        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {
            height: 450px
        }

        /* Set gray background color and 100% height */
        .sidenav {
            padding-top: 20px;
            background-color: #f1f1f1;
            height: 100%;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        img {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 70%;
            height: 100px;
            clip: rect(0px, 60px, 200px, 0px);
        }


        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }

            .row.content {
                height: auto;
            }
        }
    </style>
</head>

<body>
    <!-- Nav Bar Code -->
    <div id="nav-placeholder">
        </div>
        <script>
        $(function() {
        $("#nav-placeholder").load("nav.php");
            });
    </script>

    <div class="container-fluid">
        <!-- code for Carousel -->
        <div id="carousel1" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel1" data-slide-to="0" class="active"></li>
                <li data-target="#carousel1" data-slide-to="1"></li>
                <li data-target="#carousel1" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="appusage.jpg">
                    <div class="carousel-caption">
                        <h2>Analyze App Usage</h2>
                    </div>
                </div>
                <div class="item">
                    <img src="locationusage.jpg">
                    <div class="carousel-caption">
                        <h2>Analyze Location Data</h2>
                    </div>
                </div>
                <div class="item">
                    <img src="manageApp.jpg">
                    <div class="carousel-caption">
                        <h2>Manage User Accounts and Location Inputs</h2>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev" id="navigators">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel1" role="button" data-slide="next" id="navigators">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <!-- code for page content -->
    <div class="container-fluid text-center">
        <div class="row content">
            <!-- code for side bar page links -->
            <div class="col-sm-2 sidenav">
                <p><a href="AppUsageAnalysis.php">App Usage Analysis</a></p>
                <p><a href="LocationAnalysis.php">Location Analysis</a></p>
                <p><a href="ManageApp.php">Manage Application</a></p>
            </div>
            <!-- code for Welcome paragraph -->
            <div class="col-sm-8 text-left">
                <h1>Welcome Admin!</h1>
                <p>While the purpose of this application is to make student's campus experiences better, it is also to provide valuable feedback to
                    the institution. From this website you can recognize overcrowding, underutilized facilities and allow campuses to recognize changes needed
                    to be made to reflect growing student populations. The data collected by student's help you make decisions on campus growth and
                    improvement. From this page you can analyze user feedback in the App Usage Analysis Page and analyze location traffic levels on the Location
                    Analysis Page. You are also able to manage user accounts and location entries. </p>
                <hr>
            </div>
        </div>
    </div>
</body>

</html>
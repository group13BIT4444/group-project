<!doctype html>

<html>
    <head>
        <title>Availability Estimate</title>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="homePage.css" rel="stylesheet" />
        <script src="jquery-3.1.1.min.js"></script>

        <script> 
            function getContent(){
                var asyncRequest;
                var z = document.getElementById("contentArea");
                var location = document.forms[0].location.value;
                var selectTime = document.forms[0].selectTime.value;
                var url="availSubmit.php?location="+location&"selectTime="+selectTime;

                try {
                    asyncRequest = new XMLHttpRequest();  //create request object
                    asyncRequest.onreadystatechange=stateChange;
                    asyncRequest.open('GET',url,true);  // prepare the request
                    asyncRequest.send(null);  // send the request
                }
                catch(exception) {alert("Please Try Again");
                }
 
		    function stateChange() {
			    if(asyncRequest.readyState==4 && asyncRequest.status==200) {
				    document.getElementById("contentArea").innerHTML = asyncRequest.responseText;
			    }
            }
            }
        </script>
    </head>

    <body>
        <!--Load the navbar and it's CSS-->
        <div class="container-fluid">
            <div id="nav-placeholder">

            </div>

            <script>
                $(function() {
                    $("#nav-placeholder").load("nav.php");
                });
            </script>
        </div>

        <div class="container-fluid text-center">
            <div class="col-sm-12 sidenav text-center">
                <form action="availSubmit.php" method="get">
                    <label for="location">Select a Location: </label>
                    <select id="location" name='location'>
                        <option value="" selected disabled>Select a Location</option>
                        <?php
                            require_once("db.php");
                            $sql = "select * from location";
                            $result = $mydb->query($sql);
                            while ($row=mysqli_fetch_array($result)){
                                echo "<option id=".$row["location_name"]." value=".$row["location_id"].">".$row["location_name"]." Floor ".$row["floor"]."</option>";
                            }
                        ?>
                    </select> <br> <br>

                    <label for="selectTime">Select a Time: </label>
                    <input name="selectTime" type="time" id="selectTime"> <br><br>
                    <input type="submit" name="submit" value="Submit" class="submit"></input>
                </form>

                <div id="contentArea">
                    &nbsp;
                </div>
            </div>
        </div>
    </body>

</html>
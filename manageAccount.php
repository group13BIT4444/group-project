<?php

?>

<!DOCTYPE html>

<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <meta charset="utf-8">
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link href="loginPage.css" rel="stylesheet" />
</head>

<body>
  <div id="nav-placeholder">

  </div>

  <script>
    $(function() {
      $("#nav-placeholder").load("nav.php");
    });
  </script>

  <div align="center" class="loginBox">
    <div style="width:300px; border: solid 1px #333333; " align="left">
      <div style="background-color:#333333; color:#FFFFFF; padding:3px;"><b>Manage Account</b></div>
      <div style="margin:30px">

      <ul class="list-group list-group-flush">
  <li class="list-group-item"> <a href=editInfo.php>Edit Personal Information</li>
  <li class="list-group-item" ><a href="pointBalance.php" >Check Point Balance</li>
  <li class="list-group-item"> <a href="checkUsage.php">Check Past Usage</li>
</ul>
      </div>
    </div>
  </div>
  <br>
  <a href="deleteAccount.php" onclick="return confirm('Are you sure?');">
    <div align="center">Click here to delete your account</div>
  </a>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src='d3.v4.min.js'></script>
    <meta charset="utf-8">
    <title>Monthly Submissions</title>
    <!-- Styling for Bar chart -->
    <style>
        .bar {
            fill: steelblue;
        }

        .bar:hover {
            fill: brown;
        }

        .axis--x path {
            display: none;
        }
    </style>
    <!-- Error Label styling -->
    <style>
        .errlabel {
            color: red
        }

        ;
    </style>

</head>


<body>
    <div class="container-fluid">
        <!-- Nav Bar Code -->
        <div id="nav-placeholder">
        </div>
        <script>
            $(function() {
                $("#nav-placeholder").load("nav.php");
            });
        </script>
        <!-- code for page content -->
        <!-- Code for Bar Chart -->

        <div class="col-xs-12 col-lg-6" id="chartArea">
            <h3>Monthly Spaced Out Submissions : <h3>

                    <svg width='600' height='300'></svg>
                    <script>
                        var svg = d3.select('svg'),
                            margin = {
                                top: 20,
                                right: 20,
                                bottom: 30,
                                left: 40
                            },
                            width = +svg.attr('width') - margin.left - margin.right,
                            height = +svg.attr('height') - margin.top - margin.bottom;

                        var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
                            y = d3.scaleLinear().rangeRound([height, 0]);
                        console.log((3));

                        var g = svg.append("g")
                            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

                        d3.json('getUsageData.php', function(error, data) {
                            if (error) throw error;

                            data.forEach(function(d) {
                                d.letter = d.month;
                                d.frequency = +d.inputs;
                            })

                            console.log(data);

                            if (error) throw error;

                            x.domain(data.map(function(d) {
                                return d.letter;
                            }));
                            y.domain([0, d3.max(data, function(d) {
                                return d.frequency;
                            })]);

                            g.append('g')
                                .attr('class', 'axis axis--x')
                                .attr('transform', 'translate(0,' + height + ')')
                                .call(d3.axisBottom(x));

                            g.append('g')
                                .attr('class', 'axis axis--y')
                                .call(d3.axisLeft(y).ticks(4, 's'))
                                .append('text')
                                .attr('transform', 'rotate(-90)')
                                .attr('y', 6)
                                .attr('dy', '0.71em')
                                .attr('text-anchor', 'end')
                                .text('Frequency');

                            g.selectAll('.bar')
                                .data(data)
                                .enter().append('rect')
                                .attr('class', 'bar')
                                .attr('x', function(d) {
                                    return x(d.letter);
                                })
                                .attr('y', function(d) {
                                    return y(d.frequency);
                                })
                                .attr('width', x.bandwidth())
                                .attr('height', function(d) {
                                    return height - y(d.frequency);
                                });

                            // text label for the x axis
                            g.append("text")
                                .attr("transform",
                                    "translate(" + (width / 2) + " ," +
                                    (height + margin.top + 10) + ")")
                                .style("text-anchor", "middle")
                                .style("font-size", "14")
                                .text("Date");

                            // text label for the y axis
                            g.append("text")
                                .attr("transform", "rotate(-90)")
                                .attr("y", -4 - margin.left)
                                .attr("x", 0 - (height / 2))
                                .attr("dy", "1em")
                                .style("text-anchor", "middle")
                                .style("font-size", "14")
                                .text("Number of Inputs");
                        });
                    </script>
        </div>
    </div>
</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <style>
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }
    </style>

</head>


<body>
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Spaced Out</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="Home.php">Home</a></li>
                <?php
                session_start();
                if (isset($_SESSION["isAdmin"])) {
                    if ($_SESSION['isAdmin'] == 0) {
                        echo "<li><a href='input.php'>Input Information</a></li>";
                    }
                } else {
                    echo "<li><a href='input.php'>Input Information</a></li>";
                }
                ?>
                <li><a href="viewLocations.php">Location Viewer</a></li>
                <li><a href="compareSpaces.php">Compare Spaces</a></li>
                <li><a href="availEstimate.php">Availability Estimate</a></li>
                <li><a href="about.html">About Team</a></li>
                <?php
                if (isset($_SESSION["isAdmin"])) {
                    echo "<li class='nav-item dropdown'>";
                    if ($_SESSION["isAdmin"] == 1) {
                        echo "<a class='nav-link dropdown-toggle' href='AdminHome.php' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            Admin</a>";
                        echo "<div class='dropdown-menu' aria-labelledby='navbarDropdown'>";
                        echo "<a class='dropdown-item' href='AdminHome.php'>Admin Home Page</a><div class='dropdown-divider'></div>";
                        echo "<a class='dropdown-item' href='AppUsageAnalysis.php'>Usage Analysis</a><div class='dropdown-divider'></div>";
                        echo "<a class='dropdown-item' href='LocationAnalysis.php'>Location Analysis</a><div class='dropdown-divider'></div>";
                        echo "<a class='dropdown-item' href='ManageApp.php'>Manage Application</a>";
                    }
                }
                ?>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <?php
                    if (isset($_SESSION['account_id'])) {
                        echo "<a href='manageAccount.php'>Manage Account</a>";
                    } else {
                        echo "<a href='createAccount.php'>Create Account</a>";
                    }
                    ?>
                </li>
                <li>
                    <?php
                    if (isset($_SESSION['account_id'])) {
                        echo "<a href='logout.php'><span class='glyphicon glyphicon-log-out'></span> Logout</a>";
                    } else {
                        echo "<a href='login.php'><span class='glyphicon glyphicon-log-in'></span> Login</a>";
                    }
                    ?>
                </li>
            </ul>
        </div>
    </nav>
</body>

</html>
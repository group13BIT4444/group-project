<?php
  require_once("db.php");

  $sql = "select DATE(datetimeStamp) as day, count(input_id) as inputs from INPUT group by day order by day";

  $result = $mydb->query($sql);

  $data = array();
  for($x=0; $x<mysqli_num_rows($result); $x++) {
    $data[] = mysqli_fetch_assoc($result);
  }
  
  echo json_encode($data);
 ?>

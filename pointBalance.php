<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <meta charset="utf-8">
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="loginPage.css" rel="stylesheet" />
    
</head>

<body>
    <div id="nav-placeholder">

    </div>

    <script>
    $(function() {
        $("#nav-placeholder").load("nav.php");
    });
    </script>

    <div align="center" class="loginBox">
        <div style="width:300px; border: solid 1px #333333; " align="left">
            <div style="background-color:#333333; color:#FFFFFF; padding:3px;"><b>Check Point Balance</b></div>
            <div style="margin:30px">

                <div class="progress">
                    <?php
                    session_start();
                    $pointBalance = 0;
                    if (isset($_SESSION['point_balance'])) {
                        $pointBalance = $_SESSION['point_balance'];
                    }

                    ?>
                    <div class="progress-bar" role="progressbar" <?php echo "style='width: " . $pointBalance . "%'" ?>
                        aria-valuenow="<?php echo $pointBalance ?>" aria-valuemin="0" aria-valuemax="20">
                        <?php echo $pointBalance ?>
                    </div>
                </div>

                <h1>
                <?php
                 echo $pointBalance." points";
                 ?>
                </h1>


            </div>
        </div>
</body>

</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <title>Manage Application</title>
    <!-- Big table styling -->
    <style>
            table.greyGridTable {
        font-family: "Arial", Helvetica, sans-serif;
        border: 2px solid #FFFFFF;
        width: 100%;
        text-align: center;
        border-collapse: collapse;
        }
        table.greyGridTable td, table.greyGridTable th {
        border: 1px solid #FFFFFF;
        padding: 3px 4px;
        }
        table.greyGridTable tbody td {
        font-size: 13px;
        }
        table.greyGridTable td:nth-child(even) {
        background: #EBEBEB;
        }
        table.greyGridTable thead {
        background: #FFFFFF;
        border-bottom: 4px solid #333333;
        }
        table.greyGridTable thead th {
        font-size: 15px;
        font-weight: bold;
        color: #333333;
        text-align: center;
        border-left: 2px solid #333333;
        }
        table.greyGridTable thead th:first-child {
        border-left: none;
        }

        table.greyGridTable tfoot {
        font-size: 14px;
        font-weight: bold;
        color: #333333;
        border-top: 4px solid #333333;
        }
        table.greyGridTable tfoot td {
        font-size: 14px;
        }
    </style>
    <!-- Small table styling -->
    <style>
            table.smallGreyGridTable {
        border: 2px solid #FFFFFF;
        width: 100%;
        text-align: center;
        border-collapse: collapse;
        }
        table.smallGreyGridTable td, table.smallGreyGridTable th {
        border: 1px solid #FFFFFF;
        padding: 3px 4px;
        }
        table.smallGreyGridTable tbody td {
        font-size: 10px;
        }
        table.smallGreyGridTable td:nth-child(even) {
        background: #EBEBEB;
        }
        table.smallGreyGridTable thead {
        background: #FFFFFF;
        border-bottom: 4px solid #333333;
        }
        table.smallGreyGridTable thead th {
        font-size: 13px;
        font-weight: bold;
        color: #333333;
        text-align: center;
        border-left: 2px solid #333333;
        }
        table.smallGreyGridTable thead th:first-child {
        border-left: none;
        }

        table.smallGreyGridTable tfoot td {
        font-size: 14px;
        }
    </style>    
</head>

<!-- Submit Button Ajax -->
<script> 
    function getContent(){
        var asyncRequest;
        var z = document.getElementById("contentArea");
        var accountid = document.forms[0].accountid.value;
        var studentid = document.forms[0].studentid.value;
        var fname = document.forms[0].fname.value;
        var lname = document.forms[0].lname.value;
        var pwd = document.forms[0].password.value;
        var username = document.forms[0].username.value;
        var accountbalance = document.forms[0].accountbalance.value;
        var deleteaccount = document.forms[0].deleteaccount.value;
        var makeadmin = document.forms[0].makeadmin.value;


        var accountid = document.forms[1].accountid.value;
        var fname = document.forms[1].fname.value;
        var lname = document.forms[1].lname.value;
        var pwd = document.forms[1].password.value;
        var username = document.forms[1].username.value;
        var accountbalance = document.forms[1].accountbalance.value;
        var deleteaccount = document.forms[1].deleteaccount.value;

        var locationid = document.forms[2].locationid.value;
        var locname = document.forms[2].locname.value;
        var address = document.forms[2].address.value;
        var floor = document.forms[2].floor.value;
        var deletelocation = document.forms[2].deletelocation.value;
        var addlocation = document.forms[2].addlocation.value;


        try {
          asyncRequest = new XMLHttpRequest();  //create request object
          asyncRequest.onreadystatechange=stateChange;
          var url="ManageAppSubmit.php?accountid="+accountid&"studentid="+studentid&"fname"+fname&"lname"+lname&"username"+username&"pwd"+pwd
            &"accountbalance"+accountbalance&"deleteaccount"+deleteaccount&"makeadmin"+makeadmin&"locationid="+locationid&"locname="+locname&"address="+address
            &"floor"+floor&"deletelocation"+deletelocation&"addlocation"+addlocation;
          asyncRequest.open('GET',url,true);  // prepare the request
          asyncRequest.send(null);  // send the request
        }
        catch(exception) {alert("Request failed");
        }
 
        function stateChange(){
            if(asyncRequest.readyState==4 && asyncRequest.status==200) {
            document.getElementById("contentArea").innerHTML=
            asyncRequest.responseText;  // places text in contentArea
        }
      }
    }
</script>

<body>
    <!-- Nav Bar Code -->
    <div id="nav-placeholder">
        </div>
        <script>
        $(function() {
        $("#nav-placeholder").load("nav.php");
            });
    </script>
    <!-- code for page content -->
    <div class="container-fluid text-center">
        <div class="row content .bg-info">
            <!-- code for side bar page links -->
            <div class="col-sm-2 .bg-info">
                <p><a href="AdminHome.php">Admin Home Page</a></p>
                <p><a href="AppUsageAnalysis.php">Usage Analysis</a></p>
                <p><a href="LocationAnalysis.php">Location Analysis</a></p>
            </div>
            <!-- code for Welcome paragraph -->
            <div class="col-sm-8 text-left .bg-info">
                <h2>Welcome to the Application Management Page!</h2>
                <p>From here you can delete, update and add accounts or locations.</p>
                <hr>
            </div>
        </div>
    </div>

    <div id="GetContent">

    <div class="container-fluid">
    <!-- Nav Bar Code -->
    <div id="nav-placeholder">
        </div>
        <script>
            $(function() {
            $("#nav-placeholder").load("nav.php");
            });
    </script>
        
        <!-- Code for Student table -->
        <H3>Students</h3>
        <table class="greyGridTable table-condensed">
            <thead>
                <tr>
                <th>Account ID</th>
                <th>Student ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
                <th>Password</th>
                <th>Member Since</th>
                <th>Account Balance</th>
                </tr>
            </thead>
            <tbody>
            <?php
                require_once("db.php");
                $sql = "SELECT * FROM LOGIN INNER JOIN spacedout.student ON spacedout.student.account_id = spacedout.login.account_id WHERE isadmin = 0 GROUP BY login.account_id ORDER BY login.account_id";
                $result = $mydb->query($sql);
                while ($row =  mysqli_fetch_array($result)){
                    echo "<tr>";
                    echo "<td>".$row['account_id']."</td>";
                    echo "<td>".$row['student_id']."</td>";
                    echo "<td>".$row['fname']."</td>";
                    echo "<td>".$row['lname']."</td>";
                    echo "<td>".$row['uname']."</td>";
                    echo "<td>".$row['pwd']."</td>";
                    echo "<td>".$row['account_created']."</td>";
                    echo "<td>".$row['point_balance']."</td>";
                    echo "</tr>";

                }
            ?>
            </tbody>
            </tr>
        </table>
        <br>

        <!-- Code for Student form -->
        <form method="get" action="ManageAppSubmit.php">
            <table class="smallGreyGridTable table-condensed">
                <thead>
                    <tr>
                        <th colspan="6">Choose an account to edit</th>
                    </tr>    
                    <tr>
                        <th>Account ID</th>
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Member Since</th>
                        <th>Account Balance</th>
                        <th>Delete Account</th>
                        <th>Make Admin</th>

                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select name = "accountid">
                                <option>Select an account</option>
                                <?php
                                    $array = [];
                                    $sql = "SELECT * FROM LOGIN INNER JOIN spacedout.student ON spacedout.student.account_id = spacedout.login.account_id WHERE isadmin = 0 GROUP BY login.account_id ORDER BY login.account_id";
                                    $result = $mydb->query($sql);
                                    while($row = mysqli_fetch_array($result)){
                                        echo "<option value=".$row['account_id'].">".$row['fname']." ".$row['lname']." - Account: ".$row['account_id']." </option>";
                                    }
                                ?>
                            </select>
                        </td>
                        <td> <input type="number" name="studentid" id="studentid"></td>
                        <td> <input type="text" name="fname" id="fname"></td>
                        <td> <input type="text" name="lname" id="lname"></td>
                        <td> <input type="text" name="username" id="username"></td>
                        <td> <input type="text" name="password" id="password"></td>
                        <td> Cannot Edit</td>
                        <td> <input type="number" name="accountbalance" id="accountbalance"></td>
                        <td> <input type="checkbox" name="deleteaccount" value="delete"></td>
                        <td> <input type="checkbox" name="makeadmin" value="makeadmin"></td>

                    </tr>
                    <tr>
                        <td colspan = "6"><input type="submit" name="submit" class="btn btn-default" value="Submit"></td>
                    </tr>
                </tbody>  
            </table>
        </form>

        <!-- Code for Admin table (unfinished admin table is deleted) --> 
        <H3>Admins</h3>
        <table class="greyGridTable table-condensed">
            <thead>
                <tr>
                <th>Account ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
                <th>Password</th>
                <th>Member Since</th>
                </tr>
            </thead>
            <tbody>
            <?php
                require_once("db.php");
                $sql = "SELECT * FROM LOGIN WHERE isadmin = '1'";
                $result = $mydb->query($sql);
                while ($row =  mysqli_fetch_array($result)){
                    echo "<tr>";
                    echo "<td>".$row['account_id']."</td>";
                    echo "<td>".$row['fname']."</td>";
                    echo "<td>".$row['lname']."</td>";
                    echo "<td>".$row['uname']."</td>";
                    echo "<td>".$row['pwd']."</td>";
                    echo "<td>".$row['account_created']."</td>";
                    echo "</tr>";

                }
            ?>
            </tbody>
            </tr>
        </table>

        <!-- Code for Admin form -->
        <form method="get" action="ManageAppSubmit.php">
            <table class="smallGreyGridTable table-condensed">
                <thead>
                    <tr>
                        <th colspan="6">Choose an account to edit</th>
                    </tr>    
                    <tr>
                        <th>Account ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Member Since</th>
                        <th>Delete Account</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select name = "accountid">
                                <option>Select an account</option>
                                <?php
                                    $array = [];
                                    $sql = "SELECT * FROM LOGIN WHERE isadmin = 1 GROUP BY login.account_id ORDER BY login.account_id";
                                    $result = $mydb->query($sql);
                                    while($row = mysqli_fetch_array($result)){
                                        echo "<option value=".$row['account_id'].">".$row['fname']." ".$row['lname']." - Account: ".$row['account_id']." </option>";
                                    }
                                ?>
                            </select>
                        </td>
                        <td> <input type="text" name="fname" id="fname"></td>
                        <td> <input type="text" name="lname" id="lname"></td>
                        <td> <input type="text" name="username" id="username"></td>
                        <td> <input type="text" name="password" id="password"></td>
                        <td> Cannot Edit</td>
                        <td> <input type="checkbox" name="deleteaccount" value="delete"></td>
                    </tr>
                    <tr>
                        <td colspan = "6"><input type="submit" name="submit" class="btn btn-default" value="Submit"></td>
                    </tr>
                </tbody>  
            </table>
        </form>

        <!-- Code for Location table -->
        <H3>Locations</h3>
        <table class="greyGridTable table-condensed">
            <thead>
                <tr>
                <th>Location ID</th>
                <th>Location Name</th>
                <th>Address</th>
                <th>Floor</th>
                </tr>
            </thead>
            <tbody>
            <?php
                require_once("db.php");
                $sql = "SELECT * FROM LOCATION";
                $result = $mydb->query($sql);
                while ($row =  mysqli_fetch_array($result)){
                    echo "<tr>";
                    echo "<td>".$row['location_id']."</td>";
                    echo "<td>".$row['location_name']."</td>";
                    echo "<td>".$row['address']."</td>";
                    echo "<td>".$row['floor']."</td>";
                    echo "</tr>";
                }
            ?>
            </tbody>
            </tr>
        </table>
        
        <!-- Code for Location form -->
        <form method="get" action="ManageAppSubmit.php">
            <table class="smallGreyGridTable table-condensed">
                <thead>
                    <tr>
                        <th colspan="6">Choose a location to edit</th>
                    </tr>    
                    <tr>
                        <th>Location ID</th>
                        <th>Location Name</th>
                        <th>Address</th>
                        <th>Floor</th>
                        <th>Delete Location</th>
                        <th>Add Location</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select name = "locationid">
                                <option>Select an account</option>
                                <?php
                                    $array = [];
                                    $sql = "SELECT * FROM location";
                                    $result = $mydb->query($sql);
                                    while($row = mysqli_fetch_array($result)){
                                        echo "<option value=".$row['location_id'].">".$row['location_name']." Floor ".$row['floor']."</option>";
                                    }
                                ?>
                            </select>
                        </td>
                        <td> <input type="text" name="locname" id="locname"></td>
                        <td> <input type="text" name="address" id="address"></td>
                        <td> <input type="number" name="floor" id="floor"></td>
                        <td> <input type="checkbox" name="deletelocation" value="delete"></td>
                        <td> <input type="checkbox" name="addlocation" value="add"></td>

                    </tr>
                    <tr>
                        <td colspan = "6"><input type="submit" name="submit" class="btn btn-default" value="Submit"></td>
                    </tr>
                </tbody>  
            </table>
        </form>
    </div>

    </div>
</body>
</html>
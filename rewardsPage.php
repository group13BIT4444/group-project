<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rewards</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <meta charset="utf-8">
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container-fluid">
        <div id="nav-placeholder">

        </div>

        <script>
            $(function() {
                $("#nav-placeholder").load("nav.php");
            });
        </script>
    </div>



    <div class="container-fluid">
        <h1>Current Point Balance</h1>
        <div class="progress">
            <?php
            session_start();
            if (isset($_SESSION['point_balance'])) {
                $pointBalance = $_SESSION['point_balance'];
            }

            ?>
            <div class="progress-bar" role="progressbar" <?php echo "style='width: " . $pointBalance . "%'" ?> aria-valuenow="<?php echo $pointBalance ?>" aria-valuemin="0" aria-valuemax="2000">
                <?php echo $pointBalance ?></div>
        </div>
        <div id="list-example" class="list-group">
            <a class="list-group-item list-group-item-action" href="#list-item-1">Gift Cards!</a>
            <a class="list-group-item list-group-item-action" href="#list-item-2">Free Dining Hall Meals</a>
        </div>
        <form action="redeemPoints.php" method="post"></form>
        <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
            <!--
            <h4 id="list-item-1">Spaced out pencil</h4>
            <p>This is a reward
                <div class="form-check">
                    <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
                </div>
            </p>
            <h4 id="list-item-2">Reward 2</h4>
            <p>This is a reward</p>
            <h4 id="list-item-3">Reward 3</h4>
            <p>This is a reward</p>
            <h4 id="list-item-4">Reward 4</h4>
            <p>This is a reward</p>-->
        </div>
    </div>
    </form>





    <div id="footer-ph">

    </div>

    <script>
        $(function() {
            $("#footer-ph").load("footer.html");
        });
    </script>

</body>

</html>
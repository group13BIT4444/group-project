<?php
session_start();

$user = "";
$pwd = "";
$fname = "";
$lname = "";
$accountid = 0;
$olduser = "";

if (isset($_POST['submit'])) {
    if (isset($_POST['uname'])) $user = $_POST['uname'];
    if (isset($_POST['npwd1'])) $pwd1 = $_POST['npwd1'];
    if (isset($_POST['npwd2'])) $pwd2 = $_POST['npwd2'];

    $olduser = $_SESSION['uname'];
    $pwd = $_SESSION['pwd'];
    $fname = $_SESSION['fname'];
    $lname = $_SESSION['lname'];

    if (!empty($user) && !empty($pwd1) && !empty($pwd2)) {

        require_once("db.php");
        $sql = "SELECT * FROM login WHERE uname='$user'";
        $result = $mydb->query($sql);

        $sql = "UPDATE login
        SET uname ='" . $user . "', pwd = '" . $pwd1 . "'
        WHERE uname ='" . $olduser . "'";
        $result = $mydb->query($sql);
        $_SESSION["uname"] = $user;
        $_SESSION["pwd"] = $pwd1;
        Header("HTTP/1.1 307 Temprary Redirect");
        Header("Location: Home.php");
    }
}

?>

<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <meta charset="utf-8">
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="loginPage.css" rel="stylesheet" />
    
</head>

<body>
    <div id="nav-placeholder">

    </div>

    <script>
        $(function() {
            $("#nav-placeholder").load("nav.php");
        });
    </script>

    <div align="center" class="loginBox">
        <div style="width:300px; border: solid 1px #333333; " align="left">
            <div style="background-color:#333333; color:#FFFFFF; padding:3px;"><b>Edit Username and Password</b></div>
            <div style="margin:30px">

                <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <label>Username:
                        <input type="text" name="uname" class="box" placeholder="Enter a new username" />
                    </label>
                    <br /><br />
                    <label>New Password:
                        <input type="password" name="npwd1" class="box" placeholder="Please enter a new password" /></label>
                    <br>
                    <label>Re-Enter New Password:
                        <input type="password" name="npwd2" class="box" placeholder="Please enter a new password" /></label>
                    <br>
                    <br>
                    <input type="submit" name="submit" value="Submit" class="submit" /><br />

                </form>

            </div>
        </div>
    </div>
</body>

</html>